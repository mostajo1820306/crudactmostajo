-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2021 at 06:07 PM
-- Server version: 8.0.20
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crudact`
--

-- --------------------------------------------------------

--
-- Table structure for table `gileads`
--

CREATE TABLE `gileads` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gileads`
--

INSERT INTO `gileads` (`id`, `name`, `role`, `sex`, `created_at`, `updated_at`) VALUES
(2, 'June Osborne', 'Handmaid', 'Female', '2021-08-30 14:58:05', '2021-08-30 14:58:05'),
(3, 'Nick Blaine', 'Driver', 'Male', '2021-08-30 14:58:05', '2021-08-30 14:58:05'),
(4, 'Serena Waterford', 'Wife', 'Female', '2021-08-30 14:58:42', '2021-08-30 14:58:42'),
(5, 'Fred Waterford', 'Commander', 'Male', '2021-08-30 14:59:05', '2021-08-30 14:59:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gileads`
--
ALTER TABLE `gileads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gileads`
--
ALTER TABLE `gileads`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
