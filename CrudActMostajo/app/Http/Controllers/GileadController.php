<?php

namespace App\Http\Controllers;

use App\Models\Gilead;
use Illuminate\Http\Request;

class GileadController extends Controller
{
    public function index()
    {
        $gilead=Gilead::latest()->paginate(5);
        return view ('gilead.index',compact('gilead'))
        ->with('i',(request()->input('page',1)-1)*5);
    
    }
    
    public function create()
    {
        return view('gilead.create');
    }
    
    
    public function store(Request $request)
    {
        $request->validate([
        'name'=>'required',
        'role'=>'required',
        'sex'=>'required',
    
    ]);
        Gilead::create($request->all());
    
        return redirect()->route('gilead.index')
        ->with('success', 'Citizen Added.');
    }
    
    
    
    public function edit(Gilead $gilead)
    {
    
        return view('gilead.edit',compact('gilead'));
    }
    
        public function update(Request $request,Gilead $gilead)
    {
        $request->validate([
    
    ]);
    
        $gilead->update($request->all());
    
        return redirect()->route('gilead.index')
        ->with('success','Person updated successfully');
    
    }
    
        public function destroy(Gilead $gilead)
    {
        $gilead->delete();
    
        return redirect()->route('gilead.index')
        ->with('success','Person deleted successfully');
    
    }
}
