@extends('gilead.layout')

@section('content')

<div class="pull-left">

	<br>
	<h2 style="text-align:center;">Gilead Citizens</h2>
	<br>


	</div>

	<div class="row">

	<div class="col-lg-12 margin-tb">
	</div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{ $message }}</p>

</div>
@endif

<table class="table table-bordered">



<tr>

	<th>No</th>
	<th>Name</th>
	<th>Role</th>
	<th>Sex</th>
	<th width="280px">Action</th>
</tr>

@foreach ($gilead as $gilead)
<tr>

	<td>{{ ++$i }}</td>
	<td>{{ $gilead->name }}</td>
	<td>{{ $gilead->role }}</td>
	<td>{{ $gilead->sex }}</td>

	<td>
	<form action="{{ route('gilead.destroy',$gilead->id) }}" method="POST">
	

	<a class="btn btn-primary" href="{{ route('gilead.edit',$gilead->id) }}">Edit</a>

	@csrf
	@method('DELETE')

	<button type="submit" class="btn btn-danger">Delete</button>

	</form>

    </td>
</tr>
@endforeach
</table>

<br> 

<div style="text-align:center;">
	<a class="btn btn-success" href="{{ route('gilead.create') }}"> Add New Citizen</a>
</div>