@extends('gilead.layout')

@section('content')

<div class="row">
	
	<div class="col-lg-12 margin-tb">
	<div class="pull-left">
	<br>
	<h2 style="text-align:center;">Edit Person Details</h2>
	<br>

</div>

	</div>
	</div>

@if ($errors->any())
	<div class="alert alert-danger">
	<strong>There were some problems with your input. <br><br>

	<ul>

	@foreach ($errors->all() as $error)

	<li>{{ $error }}</li>

	@endforeach
</ul>
</div>
@endif

<form action="{{ route('gilead.update',$gilead->id) }}" method="POST">
@csrf

 @method('PUT')
	<div class="row">

	<div class="col-xs-12 col-sm-12 col-md-12">
	<div class="form-group">

	<strong>Name: </strong>

	<input type="text" name="name" value="{{ $gilead->name }}"

	class="form-control" placeholder="Name">

</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">

	<div class="form-group">
	<strong>Role: </strong>

	<input type="text" name="role" value="{{ $gilead->role }}"
class="form-control" placeholder="Role">


</div>
</div>

<div class="col=xs=12 col-sm-12 col-md-12">
	
	<div class="form-group">
	<strong>Sex: </strong>
	<input type="text" name="sex" value="{{ $gilead->sex }}" class="form-control" placeholder="Sex">
</div>

<br>
<div style="text-align:center;">
	<a class="btn btn-primary" href="{{ route('gilead.index') }}">Back</a>

	<button type="submit" class="btn btn-primary">Submit</button>
</div>

</div>

</form>

@endsection